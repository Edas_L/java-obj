import java.util.Scanner;

public class Files {
	
	public static void main(String[] args) {
		
		String directory, exten;
	    
        System.out.println("Iveskite direktorija : ");
	    Scanner scanIn = new Scanner(System.in);
	    directory = scanIn.nextLine();
	    System.out.println("Iveskite pletini : ");
	    exten = scanIn.nextLine();
	    scanIn.close(); 
	    
	    Pathnames cl = new Pathnames();
	    
	    cl.filePath(directory, exten);
	    
	}
  
}
