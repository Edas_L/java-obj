import java.io.File;
import java.io.FilenameFilter;

public class Pathnames {
	private FilenameFilter filter;
	
	public void filePath(String directory, String exten) {
		File f = null;
		File[] paths;
		File[] paths2;
		try {
	    	f = new File(directory);
	    
	    	filter = new FilenameFilter() {
		    	@Override
		    	public boolean accept(File dir, String name) {
		    		if(name.lastIndexOf('.') > 0) {
		    			int lastIndex = name.lastIndexOf('.');
		    			String str = name.substring(lastIndex);
		    			if(str.equals(exten)) {
		    				return true;
		    			}
		    		}
		    		return false;
		    	}
		    };
		    paths = f.listFiles(filter);
		    paths2 = f.listFiles();
		    for(File path: paths) {
	            System.out.println(path);
	         }
		    for(File path: paths2) {
		    	if(path.isDirectory()) {
		    		directory = path.getPath();
		    		filePath(directory, exten);
		    	}
		    }
			}catch(Exception e){
				e.printStackTrace();
			}
	}
}
