
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
/*
10. Patikrinti ar orientuotame grafe yra ciklų./Ciklas yra iš grafo briaunų sudarytas kelias, kuris prasidedda
ir baigiasi toje pačioje viršūnėje.
 */ 
class Graph {
	
    private final int V;
    private final List<List<Integer>> adj;
	private static Scanner s;
	private static int size, n1, n2;
    
    public Graph(int V) {
        this.V = V;
        adj = new ArrayList<>(V);
         
        for (int i = 0; i < V; i++)
            adj.add(new LinkedList<>());
    }
     
    private boolean isCyclicUtil(int i, boolean[] visited, boolean[] recStack){

        if (recStack[i])
            return true;
 
        if (visited[i])
            return false;
             
        visited[i] = true;
 
        recStack[i] = true;
        List<Integer> children = adj.get(i);
         
        for (Integer c : children)
            if (isCyclicUtil(c, visited, recStack))
                return true;
                 
        recStack[i] = false;
 
        return false;
    }
 
    private void addEdge(int source, int dest){
        adj.get(source).add(dest);
    }

    private boolean isCyclic(){
        boolean[] visited = new boolean[V];
        boolean[] recStack = new boolean[V];
         
        for (int i = 0; i < V; i++)
            if (isCyclicUtil(i, visited, recStack))
                return true;
 
        return false;
    }

    public static void main(String[] args){
    	s = new Scanner(System.in);
    	System.out.println("Enter the number of elements graph should consist of:");
    	size = s.nextInt();
        Graph graph = new Graph(size+1);
        if(size > 0) {
	        for(int i = 0; i < size; i ++) {
	        	System.out.println("Enter the value of the graph " + (i + 1) + " element:");
	        	n1 = s.nextInt();
	        	System.out.println("Enter the value of the edge");
	        	n2 = s.nextInt();
	        	graph.addEdge(n1, n2);
	        	System.out.println("Element created");
	        }
	     System.out.println("Graph created");
	        if(graph.isCyclic())
	            System.out.println("____________________Graph contains cycle___________________");
	        else
	            System.out.println("____________________Graph doesn't contain cycle_________________");
	        }
        else {
        	System.out.println("Graph size should be more than 0");
        }
    }
}