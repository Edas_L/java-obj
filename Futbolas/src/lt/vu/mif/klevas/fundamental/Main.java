package lt.vu.mif.klevas.fundamental;
import java.io.IOException;
import java.util.ArrayList;

import lt.vu.mif.klevas.objects.Aikstele;
import lt.vu.mif.klevas.objects.Darbuotojas;
import lt.vu.mif.klevas.objects.File;
import lt.vu.mif.klevas.objects.Meniu;
import lt.vu.mif.klevas.objects.Position;

public class Main {

	public static void main(String[] args) throws IOException {
		Aikstele aik = Aikstele.getInstance("Sauletekio futbolo aikstele", 8, 20);
		ArrayList<String> darb = new ArrayList<String>();
		darb.add(0, "ADMIN");
		darb.add(1, "COACH");
		darb.add(2, "GUARD");
		new Darbuotojas("Edas Lakavicius","1998-05-10" , Position.OWNER);
		File.readingFromFile(aik);
		Meniu m = new Meniu();
		m.run(aik, darb);
	}
}

