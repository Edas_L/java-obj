package lt.vu.mif.klevas.objects;

import java.io.IOException;

public interface CourtMeniuInterface extends MeniuInterface {
	public void organizeMatches(String date, String homeTeam, String opponentTeam, Aikstele aik) throws IOException;
	public void buyTickets(Aikstele aik) throws IOException;
	public void printTimetable(Aikstele aik);
	public void printTeams(Aikstele aik);
}
