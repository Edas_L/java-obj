package lt.vu.mif.klevas.objects;

public enum PlayerPosition {
	GK, SW, CB, RB, LB, LWB, RWB, DM, CM, CAM, LW, RW, WF, CF;
}
