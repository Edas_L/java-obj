package lt.vu.mif.klevas.objects;

import java.util.ArrayList;

public class Komanda{
	private String name;
	private int count;
	private String coach;
	private ArrayList<Player> players = new ArrayList<Player>();
	
	public Komanda(String name, int count, String coach) {
		this.name = name;
		this.count = count;
		this.coach = coach;
	}
	
	public String getName() {
		return name;
	}
	public int getCount() {
		return count;
	}
	public String getCoach() {
		return coach;
	}
	public ArrayList<Player> getPlayer() {
	    return players;
	}
	public boolean addPlayers(Player player) {
		return players.add(player);
	}
	public boolean removeRungtynes(Player player) {
		return players.remove(player);
	}
	public Player getPlayers(int index) {
		return players.get(index);
	}
	public int getPlayersSize() {
		return players.size();
	}
}
