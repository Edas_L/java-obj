package lt.vu.mif.klevas.objects;

public enum Position {
    OWNER, ADMIN, COACH, GUARD;
}
