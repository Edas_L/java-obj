package lt.vu.mif.klevas.objects;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class File {
	private static BufferedReader bufferedReader;
	public static void readingFromFile(Aikstele aik) throws IOException {
		String fileName = "komandos.txt";
		String line = null;
		FileReader fileReader = new FileReader(fileName);
		bufferedReader = new BufferedReader(fileReader);
		int players;
		while((line = bufferedReader.readLine()) != null) {
			String word[] = line.split(" ");
			players = Integer.parseInt(word[1]);
			Komanda k = new Komanda(word[0], players, word[2]);
			aik.addKomanda(k);
			for(int i = 0; i < players; i ++) {
				line = bufferedReader.readLine();
				String word2[] = line.split(" ");
				Player p = new Player(word2[0], word2[1], k, PlayerPosition.valueOf(word2[2]));
				k.addPlayers(p);
			}				
		}
	}
	public static void writeToFile(int bilietusk, int rungtyniunr, Aikstele aik) throws IOException {
		String ch1;
		PrintWriter printWriter = new PrintWriter("bilietai.txt", "UTF-8");
		for(int i = 0; i <= bilietusk; i ++) {
			ch1 = "--------------------------------------------";
			printWriter.println(ch1);
			ch1 = i + "." + "        " + aik.getRungtynes().get(rungtyniunr - 1).getDateString() + " " + aik.getRungtynes().get(rungtyniunr - 1).getTeam1() + "-" + aik.getRungtynes().get(rungtyniunr - 1).getTeam2();
			printWriter.println(ch1);
		}
		printWriter.close();
	}
}

