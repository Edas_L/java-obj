package lt.vu.mif.klevas.objects;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Human implements Serializable {
	private String name;
	private LocalDate age;
	
	final static private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			  
	public Human(String name, String age) {
		this.name = name;
		this.age = LocalDate.parse(age, formatter);
	}
	public String getName() {
		return name;
	}

	public LocalDate getAge() {
		return age;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(LocalDate age) {
		this.age = age;
	}
	public abstract void getAssignment();
}
