package lt.vu.mif.klevas.objects;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Scanner;

import java.util.ArrayList;

public class Meniu implements CourtMeniuInterface{
	
	private Scanner s;
	private BufferedReader br;
	public < E > void printArrayList( ArrayList<E> inputArrayList ) {
			int temp  = 1;
			for(E element : inputArrayList) {
				System.out.printf(temp + ". %s ", element);
				temp ++;
			}
			System.out.println();
	}
	public void printMeniu() {
			System.out.println("0- Baigti darba");
			System.out.println("1- Isidarbinimo galimybes");
			System.out.println("2- Rungtyniu organizavimas");
			System.out.println("3- Bilietu pirkimas");
			System.out.println("4- Rungtyniu tvarkarastis");
			System.out.println("5- Spausdinti komandu sarasa");
			System.out.println("6- Spausdinti pasirinktos komandos zaideju sarasa");
			System.out.println("Iveskite pasirinkima:");
	}
	public void exitMeniu() {
		System.out.println("Programa baige darba");
	}
	private void getJob(ArrayList<String> darb, int position, String jobName, String name, String birthDate) throws IOException {
		if(!darb.isEmpty())
		{
			System.out.println("Reikalingi darbuotojai siuose pozicijose: ");
			printArrayList(darb);
			System.out.println("Kurioje is pastaruju noretumete dirbti?");
			position = s.nextInt();
			jobName = darb.get(position - 1);
			System.out.println("Iveskite savo Varda Pavarde: ");
			name = br.readLine();
			System.out.println("Iveskite gimimo metus (yyyy-mm-dd)");
			birthDate = br.readLine();
			new Darbuotojas(name, birthDate, Position.valueOf(jobName));
			System.out.println("Sveikinu, Jus idarbintas ");
			darb.remove(position - 1);
		}
		else
		{
			System.out.println("\nDeja, bet siuo metu darbo pasiulyti negalime");
		}
	}
	public void organizeMatches(String date, String homeTeam, String opponentTeam, Aikstele aik) throws IOException {
		int wrong = 1;
		int temp1 = 0, temp2 = 0, wrong2 = 0;
		System.out.println("Iveskite rungtyniu data ir laika (yyyy-mm-dd h:min)");
		date = br.readLine();
		System.out.print("Iveskite savo komandos pavadinima");
		homeTeam = br.readLine();
		System.out.println("Iveskite priesininku komandos pavadinima");
		opponentTeam = br.readLine();
		for(int i = 0; i < aik.getKomanda().size(); i ++) {
			if(aik.getKomanda().get(i).getName() == homeTeam) {
				for(int j = 0; j < aik.getKomanda().size(); j ++) {
					if(aik.getKomanda().get(j).getName() == opponentTeam) {
						temp1 = i;
						temp2 = j;
						wrong2 = -1;
					}

				}
			}
		}
		if(wrong2 == - 1) {
			System.out.println("Komandu tokiais pavadinimais nera \n");
		}
		else {
			String word1[] = date.split(" ");
			String word12[] = word1[1].split(":");
			int h1 = Integer.parseInt(word12[0]);
			int min1 = Integer.parseInt(word12[1]);
			for(int i = 0; i < aik.getRungtynes().size(); i ++) {
				String word2[] = aik.getRungtynes().get(i).getDateString().split(" ");
				String word22[] = word2[1].split(":");
				int h2 = Integer.parseInt(word22[0]);
				int min2 = Integer.parseInt(word22[1]);
				System.out.println(word1[0] + " " + word1[0].length());
				System.out.println(word2[0] + " " + word2[0].length());
				date = word1[0];
				homeTeam = word2[0];
				if(date.equals(homeTeam)) {
					if(((h1 * 60) + min1) > ((h2 * 60) + min2)) {
						if((((h1 * 60) + min1) - ((h2 * 60) + min2)) < Rungtynes.getDuration()) {
							wrong = 0;
						}
					}
					else {
						if((((h2 * 60) + min2) - ((h1 * 60) + min1)) < Rungtynes.getDuration()){
							wrong = 0;
						}
					}
				}
			}
			if(wrong == 0 || wrong2 == -1) {
				System.out.println("Netinkamai pasirinkta rungtyniu data/laikas(Nepamirskite, kad tarp rungtyniu turi buti paliktas 4h periodas");
			}
			else {
				Rungtynes r = new Rungtynes(date, aik.getKomanda().get(temp1), aik.getKomanda().get(temp2));
				System.out.println("Jusu rungtynes sekmingai pridetos i tvarkarasti");
				aik.addRungtynes(r);
			}
		}
	}
	public void buyTickets(Aikstele aik) throws IOException {
		System.out.println("Pasirinkite rungtynes");
		int n1 = s.nextInt();
		System.out.println("Pasirinkite bilietu kieki");
		int n2 = s.nextInt();
		if(aik.getRungtynes().get(n1 - 1).getBilietai(n2) > 0) {
			System.out.println("Sveikiname isigijus bilietus(Bilietus rasite bilietai.txt)");
			File.writeToFile(n2, n1, aik);
		}
		else {
			System.out.println("Neturime Jusu nurodyto kiekio bilietu i sias varzybas");
		}
	}
	public void printTimetable(Aikstele aik) {
		for(int i = 0; i < aik.getRungtynes().size(); i++) {
    		System.out.println(i + 1 + ". " +aik.getRungtynes().get(i).getDate() + " " + aik.getRungtynes().get(i).getTeam1().getName() + " VS " + aik.getRungtynes().get(i).getTeam2().getName());
    	}
	}
	public void printTeams(Aikstele aik) {
		for(int i = 0; i < aik.getKomanda().size(); i ++) {
			System.out.println(i + 1 + ". " + aik.getKomanda().get(i).getName() + ", Treneris- " + aik.getKomanda().get(i).getCoach() + ", Zaideju skaicius- " + aik.getKomanda().get(i).getCount() );
		}
	}
	private void printPlayers(Aikstele aik, String teamName) throws IOException {
		System.out.println("Iveskite norimos komandos pavadinima");
		teamName = br.readLine();
		int n2 = 0;
		for(int i = 0; i < aik.getKomanda().size(); i ++) {
			if(teamName.equals(aik.getKomanda().get(i).getName())) {
				n2 = -1;
				for(int j = 0; j < aik.getKomanda().get(i).getPlayersSize(); j ++) {
					System.out.println(j + 1 + ". " + aik.getKomanda().get(i).getPlayer().get(j).getName() + " " + aik.getKomanda().get(i).getPlayer().get(j).getAge() + " " + aik.getKomanda().get(i).getPlayer().get(j).getPlayerPosition());
				}
			}
			
		}
		if( n2 != -1) {
				System.out.println("Pasirinktos komandos nera");
		}
	}
	public void run(Aikstele aik, ArrayList<String> darb) throws IOException {
		int a = 1; 
		String ch1 = null, ch2 = null, ch3 = null;
		s = new Scanner(System.in);
		br = new BufferedReader(new InputStreamReader(System.in));
		while(a != 0) {
			printMeniu();
			a = s.nextInt();
			switch(a) 
			{
			case 0:
				exitMeniu();
			break;
			case 1:
				getJob(darb, a, ch1, ch2, ch3);
			break;
			case 2:
				organizeMatches(ch1, ch2, ch3, aik);
			break;
			case 3:
				buyTickets(aik);
			break;
			case 4:
				printTimetable(aik);
			break;
			case 5:
				printTeams(aik);
			break;
			case 6:
				printPlayers(aik, ch1);
			break;
			}
		}
	}
}