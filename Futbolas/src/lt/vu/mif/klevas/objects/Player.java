package lt.vu.mif.klevas.objects;

import java.time.LocalDate;

public class Player extends Human{
	private String name;
	private PlayerPosition position;
	private LocalDate age;
	private Komanda team;
	
	public Player(String name, String age, Komanda team, PlayerPosition position) {
		super(name, age);
		this.position = position;
		this.setTeam(team);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PlayerPosition getPlayerPosition() {
		return position;
	}
	public void setPosition(PlayerPosition position) {
		this.position = position;
	}
	public LocalDate getAge() {
		return age;
	}
	public void setAge(LocalDate age) {
		this.age = age;
	}
	@Override
	public void getAssignment() {
		position = getPlayerPosition();
	}
	public Komanda getTeam() {
		return team;
	}
	public void setTeam(Komanda team) {
		this.team = team;
	}
}
