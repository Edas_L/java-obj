package lt.vu.mif.klevas.objects;

public interface Court {
	public boolean addRungtynes(Rungtynes rungtyne);
	public boolean removeRungtynes(Rungtynes rungtyne);
	public boolean addKomanda(Komanda komanda);
}
