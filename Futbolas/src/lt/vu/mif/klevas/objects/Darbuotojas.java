package lt.vu.mif.klevas.objects;
import java.time.LocalDate;

public class Darbuotojas extends Human{
	private String name;
	private Position position;
	private LocalDate age;
	
	
	public Darbuotojas(String name, String age, Position position) {
		super(name, age);
		this.position = position;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	public LocalDate getAge() {
		return age;
	}
	public void setAge(LocalDate age) {
		this.age = age;
	}
	@Override
	public void getAssignment() {
		position = getPosition();
	}
}
