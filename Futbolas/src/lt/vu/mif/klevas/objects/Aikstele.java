package lt.vu.mif.klevas.objects;
import java.util.ArrayList;


public class Aikstele implements Court{

	private String name;
	private int begin;
	private int end;
	private ArrayList<Rungtynes> rungtynes = new ArrayList<Rungtynes>();
	private ArrayList<Komanda> komandos = new ArrayList<Komanda>();
	private static Aikstele instance = null;
	
	private Aikstele(String name, int begin, int end) {
		this.name = name;
		this.begin = begin;
		this.end = end;
	}
	public static Aikstele getInstance(String name, int begin, int end) {
		if(instance == null) {
			instance = new Aikstele(name, begin, end);
		}
		return instance;
	}
	
	public ArrayList<Rungtynes> getRungtynes() {
	    return rungtynes;
	}
	public boolean addRungtynes(Rungtynes rungtyne) {
		return rungtynes.add(rungtyne);
	}
	public boolean removeRungtynes(Rungtynes rungtyne) {
		return rungtynes.remove(rungtyne);
	}
	public Rungtynes getRungtynes(int index) {
		return rungtynes.get(index);
	}
	public int getRungtynesSize() {
		return rungtynes.size();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBegin() {
		return begin;
	}
	public int getEnd() {
		return end;
	}
	public void setBegin(int begin) {
		this.begin = begin;
	}
	public void setEnd(int end) {
		this.end = end;
	}

	public ArrayList<Komanda> getKomanda() {
		return komandos;
	}

	public boolean addKomanda(Komanda komanda) {
		return komandos.add(komanda);
	}

}
