package lt.vu.mif.klevas.objects;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Rungtynes{
	private LocalDateTime date;
	private String todate;
	private int bilietai = 300;
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	private Komanda team1;
	private Komanda team2;
	private final static int duration = 240;

	public Rungtynes(String date, Komanda team1, Komanda team2) {
		this.date = LocalDateTime.parse(date, formatter);
		this.team1 = team1;
		this.team2 = team2;
		this.bilietai = bilietai;
	}
	public Komanda getTeam1() {
		return team1;
	}
	public Komanda getTeam2() {
		return team2;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public int getBilietai(int n) {
		bilietai -= n;
		return bilietai;
	}
	public static int getDuration() {
		return duration;
	}
	public String getDateString() {
		todate = date.format(formatter);
		return todate;		
	}
}
