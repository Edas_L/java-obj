package lt.vu.mif.klevas.objects;

import java.io.IOException;
import java.util.ArrayList;

public interface MeniuInterface {
	public void run(Aikstele aik, ArrayList<String> darb) throws IOException;
	public void printMeniu();
	public void exitMeniu();
}
